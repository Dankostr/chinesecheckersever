package com.app.Game;

import com.app.Player.Player;
import com.app.Player.PlayerInterface;
import com.app.ServerBoard;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class ClassicJudge implements Judge {

    ServerBoard board;
    ArrayList<Point> possibleMoves;
    public ClassicJudge(ServerBoard board){
        this.board=board;
    }


    public boolean checkBoardState(PlayerInterface player){
        Iterator<Point> it=player.getIterator();
        while(it.hasNext()){
            Point p=it.next();
            if(board.getTitleOwner(p.x,p.y)!=player.getPlayerId())
                return false;
        }
        return true;
    }

    private boolean isThatPlayerChecker(PlayerInterface player, int x, int y){
        return board.getTitleOwner(x, y) == player.getPlayerId();
    }

    private boolean isPlayerInHisDestinationTriangle(PlayerInterface player, int x, int y){
        Iterator<Point> it=player.getIterator();
        while(it.hasNext()){
            Point p=it.next();
            if(p.x==x && p.y==y){
                return true;
            }
        }
        return false;
    }

    private String DFSTriangleSearch(PlayerInterface player,int x,int y){
        Iterator<Point> it=player.getIterator();
        ArrayList<Point>triangle= new ArrayList<>();
        LinkedList<Point> list= new LinkedList<>();
        ArrayList<Point> visitedList=new ArrayList<>();
        ArrayList<Point> stringlist= new ArrayList<>();

        while(it.hasNext()){
            triangle.add(it.next());
        }

        for(int vecx=-1;vecx<=1;vecx++){
            for(int vecy=-1;vecy<=1;vecy++) {
                if (board.getTitleOwner(x + vecx, y + vecy) == -1 && vecx!=vecy) {
                    boolean tri = false;
                    for (int i = 0; i<triangle.size(); i++) {
                        Point p = triangle.get(i);
                        if (p.x == (x + vecx) && p.y == (y + vecy)) {
                            tri = true;
                        }
                    }
                    if (tri == true) {
                        Point e = new Point(x + vecx, y + vecy);
                        stringlist.add(e);
                        visitedList.add(e);
                    }
                }
            }
        }

        for(int vectorx=-2;vectorx<=2;vectorx+=2){
            for(int vectory=-2;vectory<=2;vectory+=2){
                int possibleX=x+vectorx;
                int possibleY=y+vectory;
                if(board.getTitleOwner(x+(vectorx/2),y+(vectory/2))>-1 && vectorx!=vectory){
                    if(board.getTitleOwner(possibleX,possibleY)==-1){
                        boolean tri=false;
                        for(int i=0;i<triangle.size();i++){
                            Point p=triangle.get(i);
                            if(p.x==(possibleX) && p.y==(possibleY)){
                                tri=true;
                            }
                        }
                        if(tri==true) {
                            Point e = new Point(possibleX, possibleY);
                            stringlist.add(e);
                            visitedList.add(e);
                            list.add(e);
                        }
                    }
                }

            }
        }



        while(list.peek()!=null){
            Point p=list.poll();
            for(int vectorx=-2;vectorx<=2;vectorx+=2){
                for(int vectory=-2;vectory<=2;vectory+=2){
                    int possibleX=p.x+vectorx;
                    int possibleY=p.y+vectory;
                    if(board.getTitleOwner(p.x+(vectorx/2),p.y+(vectory/2))>-1 && vectorx!=vectory){
                        if(board.getTitleOwner(possibleX,possibleY)==-1){
                            boolean tri=false;
                            for (Point s : triangle) {
                                if (s.x == (possibleX) && s.y == (possibleY)) {
                                    tri = true;
                                }
                            }
                            Point e=new Point(possibleX,possibleY);
                            it=visitedList.iterator();
                            boolean z=true;
                            while (it.hasNext()){
                                if(it.next().equals(e)){
                                    z=false;
                                    break;
                                }
                            }
                            if(z==true && tri==true) {
                                visitedList.add(e);
                                list.add(e);
                                stringlist.add(e);
                            }
                        }
                    }

                }
            }

        }

        possibleMoves=stringlist;
        return convertArrayListToString(stringlist);
    }

    private String DFSNormalSearch(PlayerInterface player,int x,int y){
        LinkedList<Point> list= new LinkedList<>();
        ArrayList<Point> visitedList=new ArrayList<>();
        ArrayList<Point> stringlist= new ArrayList<>();
        for(int vecx=-1;vecx<=1;vecx++){
            for(int vecy=-1;vecy<=1;vecy++){
                if(board.getTitleOwner(x+vecx,y+vecy)==-1 && vecx!=vecy){
                    Point e=new Point(x+vecx,y+vecy);
                    stringlist.add(e);
                    visitedList.add(e);
                }
            }
        }

        for(int vectorx=-2;vectorx<=2;vectorx+=2){
            for(int vectory=-2;vectory<=2;vectory+=2){
                int possibleX=x+vectorx;
                int possibleY=y+vectory;
                if(board.getTitleOwner(x+(vectorx/2),y+(vectory/2))>-1 && vectorx!=vectory){
                    if(board.getTitleOwner(possibleX,possibleY)==-1){
                            Point e=new Point(possibleX,possibleY);
                            stringlist.add(e);
                            visitedList.add(e);
                            list.add(e);
                    }
                }

            }

        }
        while(list.peek()!=null){
            Point p=list.poll();
            for(int vectorx=-2;vectorx<=2;vectorx+=2){
                for(int vectory=-2;vectory<=2;vectory+=2){
                    int possibleX=p.x+vectorx;
                    int possibleY=p.y+vectory;
                    if(board.getTitleOwner(p.x+(vectorx/2),p.y+(vectory/2))>-1 && vectorx!=vectory ){
                        if(board.getTitleOwner(possibleX,possibleY)==-1){
                            Point e=new Point(possibleX,possibleY);
                            Iterator<Point> it=visitedList.iterator();
                            boolean z=true;
                            while (it.hasNext()){
                                if(it.next().equals(e)){
                                    z=false;
                                    break;
                                }
                            }
                            if(z==true) {
                                visitedList.add(e);
                                list.add(e);
                                stringlist.add(e);
                            }
                        }
                    }

                }
            }

        }
        possibleMoves=stringlist;
        return convertArrayListToString(stringlist);
    }

    private String convertArrayListToString(ArrayList<Point> pointArrayList) {
        String result = String.valueOf(pointArrayList.size());
        for(int i=0; i<pointArrayList.size(); i++) {
            Point point = pointArrayList.get(i);
            String x = String.valueOf( (int)point.getX());
            String y = String.valueOf( (int)point.getY());
            result += " " + x + " " + y;
        }
        return result;
    }

    private String movesSearch(PlayerInterface player,int x,int y){
        String lista="";
        if(isPlayerInHisDestinationTriangle(player,x,y)==true){
            lista=DFSTriangleSearch(player,x,y);
        }else{
            lista=DFSNormalSearch(player,x,y);
        }
        return lista;
    }

    public boolean makeMove(PlayerInterface player, int ox, int oy, int nx, int ny){
        if(possibleMoves!=null){
            for (Point p:possibleMoves) {
                if(p.x==nx && p.y==ny)
                    return true;
            }
        }
        return false;
    }

    public String giveCheckerMoves(PlayerInterface player,int x,int y){
        if(this.isThatPlayerChecker(player,x,y)==false){
            possibleMoves=null;
            return "0";
        }
        return movesSearch(player,x,y);
    }

    public String giveBotMoves(PlayerInterface player, int x, int y) {
        return movesSearch(player, x, y);
    }
}
