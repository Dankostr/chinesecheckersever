package com.app.Game;

import com.app.Client.ClientCommunicator;
import com.app.Player.BotPlayer;
import com.app.Player.Player;
import com.app.Player.PlayerInterface;
	import com.app.ServerBoard;

import java.awt.*;
import java.util.ArrayList;

public class Game {

	PlayerInterface[] slots=new PlayerInterface[6];
	ServerBoard board=new ServerBoard(4);
	ArrayList<ClientCommunicator>observer=new ArrayList<>();
	ArrayList<Integer> winners=new ArrayList<>();
	Judge judge=new ClassicJudge(board);
	boolean hasGameStarted=false;
	private int currentPlayerId;
	private int skip=0;


	private void resetGame(){
		hasGameStarted=false;
		for(int i=0;i<6;i++){
			if(slots[i] instanceof Player)
				setReady(((Player)slots[i]),false);
			else
				slots[i]=null;
		}
		skip=0;
		board=new ServerBoard(4);
		judge=new ClassicJudge(board);
	}

	public String botMove(){
		if(slots[currentPlayerId] instanceof  BotPlayer){
			return ((BotPlayer)slots[currentPlayerId]).makeBestMove();
		}
		return "NON_BOT";
	}

	public boolean checkState(int playerid){
		if(judge.checkBoardState(slots[playerid])==true) {
			slots[playerid].setState("Wygrał");
			winners.add(playerid);
			return true;
		}
		return false;
	}

	public boolean checkslot(int i){
		if(slots[i]==null)
			return true;
		return false;
	}

	public int leaveSlot(int i){
		slots[i]=null;

		int noOfHumanPlayers=0;
		for(int a=0;a<6;a++){
			if(slots[a]!=null && slots[a] instanceof Player){
				noOfHumanPlayers++;
			}
		}
		if(noOfHumanPlayers==0){
			resetGame();
			return -2;
		}


		if(i==currentPlayerId && hasGameStarted==true){
			changeCurrentPlayer();
			return currentPlayerId;
		}
		return -1;
	}

	private boolean joinSlot(Player player,int slotid){
		if(slots[slotid]==null && hasGameStarted==false ){
			synchronized (this){
				if(checkslot(slotid)==true && hasGameStarted==false){
					slots[slotid]=player;
					player.setPlayerId(slotid);
					player.setDestinationTriangle(board.getDestinationTriangle(slotid));
					return true;
				}
			}
		}
		return false;
	}

	public String joinGame(Player player,int slotid ){
		for(int i=0;i<6;i++){
			if(slots[i]==player)
				if(slots[slotid]==null) {
					boolean result = joinSlot(player, slotid);
					if (result == true) {
						slots[i]=null;
						return "Changed "+i;
					} else{
						return "NOT_JOINED";
					}
				}
		}
		if(joinSlot(player,slotid)==true){
			return "JOINED";
		}else{
			return "NOT_JOINED";
		}

	}
	public void setReady(Player player, boolean ready){
		player.setReady(ready);
	}

	public boolean checkIfAllready(){
		for (PlayerInterface p:slots) {
			if(p!=null && p.getReady()==false)
				return false;
		}
		return true;
	}

	private int noOfPlayers(){
		int numberOfPlayers=0;
		for(int i=0;i<6;i++){
			if(slots[i]!=null){
				numberOfPlayers++;
			}
		}
		return numberOfPlayers;
	}

	public boolean areSlotsEven(){
		int noOfPlayers=noOfPlayers();
		switch (noOfPlayers){
			case 2:
				for (int i=0;i<6;i++){
					if (slots[i]!=null){
						if(slots[5-i]==null)
							return false;
					}
				}
				return true;
			case 3:
				for (int i=0;i<6;i++){
					if (slots[i]!=null){
						if(slots[5-i]!=null)
							return false;
					}
				}
				return true;
			case 4:
				for (int i=0;i<6;i++){
					if (slots[i]!=null){
						if(slots[5-i]==null)
							return false;
					}
				}
				return true;
			case 6:
				return true;
		}
		return false;
	}

	public String getTakenSlots(){
		String list=Integer.toString(noOfPlayers());
		for(int i=0;i<6;i++){
			if(slots[i]!=null){
				list+=" "+ i;
			}
		}
		return list;
	}

	public int startGame(){
		int numberOfPlayers=noOfPlayers();
		if(numberOfPlayers==1 || numberOfPlayers==5 || !areSlotsEven()){
			for(int i=0;i<6;i++){
				if(slots[i]!=null && slots[i] instanceof Player)
				setReady(((Player)slots[i]),false);
			}
			return -2;
		}
		hasGameStarted=true;
		int a=(int)Math.random()*6;
		for(PlayerInterface player:slots){
			if(player!=null){
				player.setState("playing");

			}
		}
		setTriangles();
		while(true){

			if(slots[a]==null) {
				a = (int) (Math.random() * 6);
			}else{
				return a;
			}
		}

	}

	private void setTriangles() {
		switch(noOfPlayers()){
			case 4:
			case 2:
				for(int i=0;i<6;i++){
					if(slots[i]==null){
						ArrayList<Point>points=board.getDestinationTriangle(i);
						for(Point p:points){
							board.setTitleOwner(p.x,p.y,-1);
						}
					}
				}
				break;
			case 3:
				for(int i=0;i<6;i++){
					if(slots[i]!=null){
						ArrayList<Point>points=board.getDestinationTriangle(i);
						for(Point p:points){
							board.setTitleOwner(p.x,p.y,-1);
						}
					}
				}
		}
	}


	public String makeMove(int slotid,int x , int y ,int desc_x, int desc_y ){
		if(currentPlayerId==slotid&& hasGameStarted==true && judge.makeMove(slots[slotid],x,y,desc_x,desc_y)==true){
			board.makeMove(x,y,desc_x,desc_y);
			skip=0;
			return "SUCCESS";
		}
		return "FAILURE";
	}

	private void changeCurrentPlayer() {
		while(true){
		if(currentPlayerId%2==0){
			currentPlayerId+=2;
			if(currentPlayerId==6){
				currentPlayerId--;
			}
		}else{
			currentPlayerId-=2;
			if(currentPlayerId==-1){
				currentPlayerId++;
			}
		}
		if(slots[currentPlayerId]!=null && slots[currentPlayerId].getState().equals("playing")){
			break;
			}
		}
	}

	public String getMoves(Player player,int x,int y){
		if(currentPlayerId==player.getPlayerId() && hasGameStarted==true)
			return judge.giveCheckerMoves(player, x, y);
		else
			return "0";
	}

	public int skipTurn(int player) {
		if(currentPlayerId==player) {
			changeCurrentPlayer();
			return currentPlayerId;
		}
		return -1;
	}

	public boolean addBot(int slotid) {
		if(slots[slotid]==null && hasGameStarted==false){
			synchronized (this){
				if(checkslot(slotid)==true && hasGameStarted==false){
					slots[slotid]=new BotPlayer();
					((BotPlayer)slots[slotid]).setPlayerId(slotid);
					((BotPlayer)slots[slotid]).setDestinationTriangle(board.getDestinationTriangle(slotid));
					((BotPlayer)slots[slotid]).setServerBoard(board);
					((BotPlayer)slots[slotid]).setJudge(judge);
					return true;
				}
			}
		}
		return false;
	}

	public boolean removeBot(int slotid){
		if(slots[slotid]!=null && hasGameStarted==false && slots[slotid] instanceof BotPlayer){
			slots[slotid]=null;
			return true;
		}
		return false;
	}


	public int getBotId() {
		return currentPlayerId;
	}

	public String getWinners() {
		String winnerlist="";
		for (Integer winnerId: winners) {
			winnerlist+=" "+slots[winnerId].getName();
		}
		return winnerlist;
	}

	public boolean getNoOfWinners(){
		if(winners.size()==noOfPlayers()){
			resetGame();
			return true;
		}
		return false;
	}

	public void addSkipCounter() {
		skip++;
	}
	public boolean checkForSkipDraw(){
		int playerCount=0;
		for(int i=0;i<6;i++){
			if(slots[i]!=null && slots[i].getState().equals("playing")){
				playerCount++;
			}
		}
		if(playerCount*3==skip){
			resetGame();

			return true;
		}
		return false;
	}

}
