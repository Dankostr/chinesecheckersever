package com.app;

/**
 *
 */
public class Tile {
    /*
    tile==null <- impossible to go here
    owner=-1<- free space
    owner=i, player witht number i is owner of tile
     */
    int owner=-2;
    public Tile(int player){
        owner=player;
    }
    public void setOwner(int player){
        owner=player;
    }

    public int getOwner(){
        return this.owner;
    }
}
