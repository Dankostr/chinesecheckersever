package com.app.Client;

import com.app.server.Translator;

import java.io.IOException;
import java.net.Socket;

public class ClientConnector {

    private final static int PORT_NUMBER=2137;
    private Socket socket;
    private Thread thread;


    ClientConnector(String ipadress, Translator translator){
        try {
            socket=new Socket(ipadress,PORT_NUMBER);
        } catch (IOException e) {
            e.printStackTrace();
        }
        thread= new Thread(new ClientCommunicator(socket, translator));
        thread.start();
    }
}
