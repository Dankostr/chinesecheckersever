package com.app.Client;

import com.app.Player.Player;
import com.app.server.Translator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientCommunicator implements Runnable {

	private Socket clientSocket;
	private BufferedReader clientInput;
	private PrintWriter clientOutput;
	Player player;
	Translator translator;
	public ClientCommunicator(Socket clientSocket, Translator translator){
		this.clientSocket=clientSocket;
		this.translator = translator;
		player=new Player();
		try {
			clientInput=new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			clientOutput=new PrintWriter(clientSocket.getOutputStream(),true);
			translator.addObserver(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Player getPlayer(){
		return player;
	}

	@Override
	public void run() {
		while(true) {
			try {
				waitForMessage();
				decodeMessage(readMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void waitForMessage() throws IOException {
		while(clientInput.ready()!=true) {
			
		}
	}
	
	private String readMessage() throws IOException {
		return clientInput.readLine();
	}



	private void decodeMessage(String message) {
			sendMessage(translator.decodeMessage(this.player,message));
	}
	
	public void sendMessage(String message) {
		clientOutput.println(message);
	}
	

}
