package com.app.server;

import java.io.IOException;
import java.net.ServerSocket;

import com.app.Client.ClientCommunicator;
import com.app.Game.Game;

public class ServerLoop {
	
	private final static int PORT_NUMBER=2137;
    private static ServerSocket serverSocket;
    private static ClientCommunicator clientHandler;
    private static Thread thread;
    private static Translator translator;
    private static Game game;
    public static void main(String[] args) throws IOException {
        serverSocket = new ServerSocket(PORT_NUMBER);
        game=new Game();
        translator=new Translator(game);
        while (true) {
            clientHandler = new ClientCommunicator(serverSocket.accept(),translator);
            thread = new Thread(clientHandler);
            thread.start();
        }
    }

    protected void finalize() throws IOException {
        serverSocket.close();
    }
}
