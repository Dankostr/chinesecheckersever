package com.app.server;

import com.app.Client.ClientCommunicator;
import com.app.Game.Game;
import com.app.Player.Player;

import java.util.ArrayList;

public class Translator {

    Game game;
    ArrayList<ClientCommunicator> observer=new ArrayList<>();
    ArrayList <String>allcommands=new ArrayList<>();

    public void addObserver(ClientCommunicator cC){
        observer.add(cC);
    }

    private String sendAllCommands(){
        String commands="";
        for (String command:allcommands){
            commands+=command+"\n";
        }
        return commands;
    }

    private void notifyAllOberservs(String message){
        allcommands.add(message);
        for(ClientCommunicator clientCommunicator: observer){
            if(clientCommunicator.getPlayer().getName()!=null)
                clientCommunicator.sendMessage(message);
        }
        if(message.equals("GAME_FINISHED")){
            allcommands.clear();
        }
    }

    public Translator(Game game){
        this.game=game;
    }

    public synchronized String decodeMessage(Player player, String message){
        String arr[] = message.split(" ", 2);
        String command = arr[0];
        String arguments;
        if(arr.length>1) {
            arguments = arr[1];
        }
        else {
            arguments = null;
        }
        switch(arr[0]) {
            case "NICK":
                decodeNickCommand(player,arguments);
                return "NICK_ACCEPTED "+arguments;
            case "GET_CURRENT_STATE":
                return "ALL COMMANDS\n"+sendAllCommands();
            case "MOVE":
                return "MOVE_DONE "+decodeMoveCommand(player,arguments);

            case "MOVES":
                return "AVAILABLE_MOVES "+decodeGetMovesCommand(player,arguments);

            case "JOINSLOT":
                return "SLOT_JOIN "+decodeJoinSlot(player,arguments);

            case "LEAVESLOT":
                return "LEAVESLOT "+decodeLeaveSlot(player,arguments);
            case "PLAYER_READY":
                decodePlayerReady(player,true);
                if(game.checkIfAllready()==true){
                    int i=game.startGame();
                    if(i==-2)
                        this.notifyAllOberservs("GAME_INFO ERROR");
                    else {
                        this.notifyAllOberservs("GAME_INFO "+game.getTakenSlots());
                        this.notifyAllOberservs("GAME_READY");
                        this.notifyAboutTurn(i);
                        checkBotMove();
                    }
                }
                return "SUCCESS";
            case "PLAYER_NOT_READY":
                decodePlayerReady(player,false);
                return "SUCCESS";
            case "SKIP_TURN":
                decodeSkipTurn(player,arguments);
                break;
            case "ADD_BOT":
                if(decodeAddBot(player,arguments)==true)
                    notifyAllOberservs("BOT_ADDED "+arguments);
                break;
            case "REMOVE_BOT":
                if(decodeRemoveBot(player,arguments)==true)
                    notifyAllOberservs("BOT_REMOVED "+arguments);
                break;
            case "PLAYER_HAS_LEFT":
                    decodePlayerHasLeft(player,arguments);
        }
        return "FAIL";
    }

    private void decodePlayerHasLeft(Player player, String arguments) {
        deleteObserver(player);
        int turn=game.leaveSlot(player.getPlayerId());
        if(turn==-2){
            notifyAllOberservs("GAME_FINISHED");
        }
        if(turn>-1){
            notifyAboutTurn(turn);
        }
        notifyAllOberservs("PLAYER_LEFT "+player.getPlayerId());
        player.setPlayerId(-1);

    }
    private void deleteObserver(Player player){
        for(int i=0;i<observer.size();i++){
            if(observer.get(i).getPlayer()==player){
                observer.remove(i);
            }
        }
    }

    private void notifyAboutTurn(int i) {
        for(ClientCommunicator clientCommunicator: observer){
            if(clientCommunicator.getPlayer().getPlayerId()!=i)
                clientCommunicator.sendMessage("ENEMY_TURN");
            else
                clientCommunicator.sendMessage("YOUR_TURN");
        }
    }

    private boolean decodeRemoveBot(Player player, String arguments) {
        int slotid=Integer.parseInt(arguments);
        return game.removeBot(slotid);
    }

    private boolean decodeAddBot(Player player, String arguments) {
        int slotid=Integer.parseInt(arguments);
        return game.addBot(slotid);
    }

    private void decodeSkipTurn(Player player, String arguments) {
        int new_player=game.skipTurn(player.getPlayerId());
        game.addSkipCounter();
        if(game.checkForSkipDraw()==true){
            notifyAllOberservs("DRAW");
        }
        if(new_player!=-1){
            notifyAboutTurn(new_player);
        }
    }

    private void decodePlayerReady(Player player, boolean ready) {
        game.setReady(player,ready);
    }

    private String decodeLeaveSlot(Player player, String arguments) {
        int turn=game.leaveSlot(player.getPlayerId());
        if(turn!=-1){
            notifyAboutTurn(turn);
        }
        notifyAllOberservs("PLAYER_LEFT "+player.getPlayerId());
        player.setPlayerId(-1);
        return "SUCCESS";
    }

    private String decodeJoinSlot(Player player, String arguments) {
        int slotid=Integer.parseInt(arguments);
        String anwser=game.joinGame(player,slotid);
        if(anwser.equals("JOINED")){
            notifyAllOberservs("PLAYER_JOINED "+player.getPlayerId()+" "+player.getName());
            return String.valueOf(slotid);
        }else if(anwser.equals("NOT_JOINED")){
            return "-1";
        }else{
            String[] splitAnwser=anwser.split(" ",2);
            notifyAllOberservs("PLAYER_LEFT "+splitAnwser[1]);
            notifyAllOberservs("PLAYER_JOINED "+player.getPlayerId()+" "+player.getName());
            return String.valueOf(slotid);
        }
    }

    private void decodeNickCommand(Player player,String args){
        player.setName(args);
    }

    private String decodeGetMovesCommand(Player player,String args){
        String arr[]=args.split(" ",2);
        int x = Integer.parseInt(arr[0]);
        int y = Integer.parseInt(arr[1]);
        return game.getMoves(player,x,y);

    }

    private String decodeMoveCommand(Player player,String args){
        String arr[]=args.split(" ",4);
        int fromX = Integer.parseInt(arr[0]);
        int fromY = Integer.parseInt(arr[1]);
        int toX = Integer.parseInt(arr[2]);
        int toY = Integer.parseInt(arr[3]);
        String result= game.makeMove(player.getPlayerId(),fromX,fromY,toX,toY);
        if(result.equals("SUCCESS")){
            notifyAllOberservs("MOVE_DONE "+args);
            if(game.checkState(player.getPlayerId())==true){
                notifyAllOberservs("WINNERS"+game.getWinners());
                if(game.getNoOfWinners()==true){
                    notifyAllOberservs("GAME_FINISHED");
                }
            }
            int a=game.skipTurn(player.getPlayerId());
            notifyAboutTurn(a);
            checkBotMove();

        }
        return result;
    }

    private void checkBotMove(){
        while(true){
            String message=talkToBot();
            if(message.equals("END")){
                break;
            }else if(message.equals("SKIP")){
                int turn=game.skipTurn(getBotId());
                notifyAboutTurn(turn);
            }
            else{
                notifyAllOberservs(message);
                if(game.checkState(getBotId())==true){
                    notifyAllOberservs("WINNERS"+game.getWinners());
                    if(game.getNoOfWinners()==true){
                        notifyAllOberservs("GAME_FINISHED");
                        return ;
                    }
                }
                int turn=game.skipTurn(getBotId());
                notifyAboutTurn(turn);
            }
        }
    }

    private String talkToBot(){
        String a=game.botMove();
        if(a.equals("NON_BOT")){
            return "END";
        }
        return a;
    }

    private int getBotId(){
        return game.getBotId();
    }
}
