package com.app.boardtest;

import com.app.ServerBoard;
import org.junit.Assert;
import org.junit.Test;

public class ExampleTest {

    @Test
    public void TestBoardSize() {
        ServerBoard board = new ServerBoard(7);
        Assert.assertTrue(board.getBoardSizeX() > 0);
        Assert.assertTrue(board.getBoardSizeY() > 0);
    }

}
